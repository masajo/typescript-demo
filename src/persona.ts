// Ejemplo de cómo exportar una clase Persona
export class Persona {
  nombre: string;
  email: string;

  constructor(nombre:string, email:string) {
    this.nombre = nombre;
    this.email = email;
  }

}

// Ejemplo de cómo exportar una función SUMAR
export const sumar = (x: number, y: number):number => {
  return x + y
}