// Mi primer archivo javascript
console.log('Hola Mundo');
console.log("Hola Mundo");

var nombre = "Martín"; // VAR: string, está pensado para variables de ámbito global en el JS
console.log("Hola " + nombre);
console.log("Qué tal",nombre);
console.log(`¿Cómo han ido las vacaciones, ${nombre}`);

let email = "martin@imaginagroup.com"; // LET: string, está pensado para variables de ámbito cerrado o scope cerrad0
console.log(`Email de ${nombre}: ${email}`);

const pi = 3.1416; // CONST: number un valor que no puede cambiar
// ! Error: Las constantes no se pueden sobrescribir
// ! pi = pi + 1;

/**
 * Esto es un comentario multilínea
 * Es decir, podemos 
 * escribir en varias líneas
 */

var error = 404 // number
error = "No se ha encontrado" // string
error = true // boolean
console.log(`El error: ${error}`) // Al ser un tipo inferido, puede cambiar a lo largo del script

let x, y, z; // instanciación
x = 1; // incialización
y = 2; 
z = 3;
a = 4;

console.log(`Resultado Suma: ${x+y+z}`)
console.log(`Resultado Resta: ${x-y-z}`)
console.log(`Resultado Mult: ${x*y*z}`)
console.log(`Resultado Elevado: ${z**y}`) // Elevado A... z^y
console.log(`Resultado Mult: ${a/y}`) // División
console.log(`Resultado Resto: ${a%y}`) // Resto

if (a%2==0) {
    console.log('Es Par');
} else {
    console.log('Es Impar');
}

/**
 * Función clásica que sirve para saludar por consola a un nombre
 * @param {string} nombre 
 */
function saludar(nombre) {
    console.log(`Hola, ${nombre}`)
}

// Llamada a una función
saludar("Pepe");

/**
 * Función Lambda / Anónima, asignada a una
 * constante llamada "despedirse"
 * @param {string} nombre 
 */
const despedirse = (nombre) => {
    console.log(`Adiós, ${nombre}`)
}

// LLamada a una función lambda
despedirse('Pepe');

/**
 * 
 * @param {string} nombre 
 * @param {function} despedirse --> Función a ejecutarse dentro de saludo_despedida
 */
function saludo_despedida(nombre, callback) {
    console.log('Hola', nombre) // Hola, Julián
    callback(nombre); // Adiós, Julián --> El parámetro era una función con parámetros
}

//A saludo_despedida le pasamos un string y una función anónima definida como
// se establece y recibe un parámetro llamado nombre
saludo_despedida('Julián', (persona) => {console.log('Adiós', persona)})


let respuesta = "El resultado es '2' "; // Podemos embeber comillas doblesy simples
respuesta = 'El resultado es "2" ';
respuesta = `El resultado es ${2}`; // El resultado es 2

console.log(respuesta.replace('E', 'A'))
console.log(respuesta.indexOf('r')) // 3
console.log(respuesta.indexOf('x')) // -1
console.log('Tamaño texto:', respuesta.length) // 14 chars

// SLICE: Desde una posición, hasta otra, te devuelve lo que haya dentro
let persona = "Martín San José";
console.log(persona.slice(0, 6)) // desde 0 - 6 (incluido)

var valorNull = null
var valorUndefined = undefined
var valorCero = 0
var valorFalse = false

// null, undefined, 0 y false --> false en un IF
// Estos son valores no son válidos en comparaciones
if(!valorNull && !valorUndefined && !valorCero && !valorFalse){
    console.log("Valores que javascript toma como NADA o Falsos")
}

if(!persona){
    console.log("Persona es TRUE, Not Null, No 0 o Defined")
}else{
    console.log("La persona no está definida")
}


var listaCompra = ["Manzana", "Tomates", false, 5];

console.log('Total Compra: ', listaCompra[3]*2);

if(listaCompra[2]){
    console.log("Ha terminado de comprar");
}else console.log("Sigue Comprando...");


var listaVerduras = ["Lechuga", "Tomate", "Pepino"]

// Para hacer bucles, es mejor hacer uso de foreach o map
listaVerduras.forEach((elemento, posicion) => {
        console.log(`Has comprado: ${elemento} en la posición: ${posicion}`);
    }
);

// Objecto --> Se parece mucho a un JSON
// JSON --> JAVASCRIPT OBJECT NOTATION
persona = {
    nombre: "Martín",
    edad: 29,
    email: "martin@imaginagroup.com"
}

console.log(persona.nombre, ' tiene: ', persona.edad, ' años')


// TODO: 1. Solicitar al usuario un número por consola y saber si es par o no
// TODO: 2. Solicitar al usuario un número por consola y saber si es primo o no


let contacto1 = {
    nombre: 'Juan',
    telefono: '653123'
}

let contacto2 = {
    nombre: 'Alba',
    telefono: '653142'
}

let contacto3 = {
    nombre: 'Anabel',
    telefono: '123576'
}

let listaContactos = [contacto1, contacto2];

// Factor de propagación (...variable)
let lista2Contactos = [...listaContactos, contacto3 ] // [contacto1, contacto2, contacto3]

lista2Contactos.forEach((contacto) => {
    console.log(`${contacto.nombre}, Tel: ${contacto.telefono}`)
});


let estadoAplicacion = {
    usuario: 'admin',
    email: 'admin@admin.com',
    logged: false
}


let nuevoEstadoAplicacion = {
    // usuario: 'admin,
    // email: 'admin@admin.com'
    ...estadoAplicacion, // replica todos los elementos de estadoAplicación --> usuario, email
    logged: true, // pero actualiza la clave "logged"
    direccion: 'Valencia' // y añade la nueva clave 'direccion'
}

console.table(nuevoEstadoAplicacion) // muestra tabla de claves y valores

// Mostrar en una tabla una lista de objetos
let listaEstados = [nuevoEstadoAplicacion,nuevoEstadoAplicacion,nuevoEstadoAplicacion]
console.table(listaEstados)

let listaRecados = ["Comprar", "Lavar el coche"]

// Obtener último elemento
let ultimorecado = listaRecados.pop() // "Lavar el coche"

// Añadimos un elemento a una lista
listaRecados.push("Practicar JS");

// TODO: Investigar: SHIFT Y UNSHIFT ()
// TODO: INVESTIGAR CÓMO ELIMINAR UN ELEMENTO POR ID (desde dónde y cuántos)

// * Recomendación: Visitar 
// * https://www.w3schools.com/js/default.asp
