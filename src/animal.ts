// Cuando exportamos haciendo uso de "default"
// luego se importa sin las {}
// Solo debe haber un default por archivo

export default interface IAnimal {
  nombre: string;
  tipo: string;
  dob: Date;
}
