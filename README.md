# Typescript-Demo

Proyecto de ejemplo para empezar a desarrollar con Typescript

En este proyecto aprenderemos a usar operadores, funciones, bucles, clases, interfaces, enumerados, importación y exportación para que cuando pasemos a Angular, la mayoría de los fundamentos de ES6 en adelante y TypeScript estén dominados con soltura.
