import { Persona, sumar } from './persona'; // Con las {} nos importamos aquellos elementos que se exportan
import Perro from './animal'; // Le puedo poner el nombre que yo quiera, no se usan los {}
// Importante, por lo tanto, importar siempre con un nombre adecuado para evitar confusiones

console.log('Hola, Bienvenido a Typescript')

// Usando la clase exportada de persona.ts
const contacto1 = new Persona('Martín', 'martin@imaginagroup.com');

console.log(contacto1.nombre, contacto1.email)

// Usando la función exportada de persona.ts
console.log('1 + 2 =', sumar(1, 2));

// rex es un Objecto que sigue/implementa la interface Perro
// por lo tanto, tiene que tener nombre, tipo y dob (fecha de nacimiento)
// Aquí usamos la Interface como un Tipo
const rex: Perro = {
  nombre: 'Rex',
  tipo: 'Perro',
  dob: new Date()
}

console.log(rex.nombre);

const limpiar = (producto:string):string => "Estoy limpiando con " + producto;

const accion = limpiar("Mistol");


const comer = (comida: string): string => {
  const kilos = 1;
  const nombre = 'Martín'
  console.log(`${nombre} ha comido ${kilos} de ${comida}`)
  return `Estoy comindo ${comida}`
}

const listaCompra: string[] = ['patatas', 'leche']

for (let index = 0; index < listaCompra.length; index++) {
  const element = listaCompra[index];
}

listaCompra.forEach((elemento: string, index: number) => {
  console.log(elemento, index)
})

// eslint-disable-next-line no-var
for(var i = 0; i < 1; i += 1) {
  console.log("Bucle de una vuelta (i) para obtener: ", i);
}

for(let j = 0; j < 1; j += 1) {
  console.log("Bucle de una vuelta (j) para obtener: ", j);
}

console.log(i) // podría imprimir el i por que es un VAR
// console.log(j) // ERROR: J no es accesible ya que es un LET

// VAR --> Variables de ámbito globales
// LET --> Variables de ámbito local o bloque (bloque global o bloque local)
// CONST --> Constantes de ámbito local o bloque (bloque global o bloque local)

// Una función con un único parámetro de entrada
const agregar1 = (elementos:number[]) => {
  elementos.forEach(() => {
    console.log(elementos)
  })
}

// Una función con N parámetros de entrada
const agregar2 = (...elementos: number[]) => {
  elementos.forEach(() => {
    console.log(elementos)
  })
}


// Parámetro = [1,2,3]
agregar1([1, 2, 3]);

// Parámetros son 1,2,3,4,5,6,7 y la función los trata como [1,2,3,4,5,6,7]
agregar2(1, 2, 3, 4, 5, 6, 7);

const lista1: number[] = [1, 2, 3]
const lista2: number[] = [...lista1, 4, ...lista1]

agregar2(...lista2) // Paso de cada uno de los elementos de lista2 como parámetros (1,2,3,4,1,2,3)


const multiplicar = (x:number, y = 2): number => x * y

multiplicar(3) // 6 ya que la Y no se pasa y toma el 2 como valor por defecto
multiplicar(y=2, x=3) // 6 pasamos los parámetros en otro oden, pero indicando el nombre del parámetro

const foo = ['uno', 'dos', 'tres'];
const [uno, dos, tres] = foo; // genera 3 variables

console.log(uno); // 'uno'

const { nombre, tipo, dob } = rex // otra manera de usar destructuring para obtener los valores
console.log('Nombre:', nombre)


const miModuloMatematicas = {
  sumatorio: function sumatorio(...x: number[]) {
    let solucion = 0
    x.forEach((elemento: number) => {
      solucion += elemento;
    });
    return sumatorio;
  },
  restar: function restar(x: number, y:number) {
    return x - y
  },
  dividir: function dividir(x: number, y:number) {
    return x / y
  },
};


const {sumatorio, restar, dividir} = miModuloMatematicas; // creamos dos constantes a partir de las claves del objeto
sumatorio(5, 5, 4, 5, 6, 7);
restar(3, 2);
dividir(1, 6);


const miPersona = {
  perfil: {
    nombre: "",
    edad: 0,
    direccion: undefined
  }
};

// Operador de manejo de Nulos, Undefined y valores no válidos
console.log(miPersona.perfil.nombre || "Anónimo"); // Anónimo
console.log(miPersona.perfil.edad || 18); // 18
console.log(miPersona.perfil.direccion || "Desconocida"); // Desconocida

console.log(miPersona.perfil.nombre ?? "Anónimo"); // ""
console.log(miPersona.perfil.edad ?? 18); // 0

// OPerador Ternario
const conectado = true;
console.log(conectado ? 'Está conectado': 'Está desconectado')


// TUPLA
const error: [string, number] = ["Problema en el servidor", 500];

// ENUMERADOS
// El valor por defecto, es su posición
// SI queremos cambiar los valores, tenemos que hacerlo a través de =
enum Error { Servidor, Cliente }
enum Color { Rojo = 'Rojo', Verde = 'Verde', Azul = 'Azul' }
enum Pais { Es = 2, Fr, De} // Es=2, Fr=3, De=4

const errorS: Error = Error.Servidor // 0 --> Por defecto la primera posición es 0
const verde: Color = Color.Verde; // 'Verde' -->Le hemos dado a Verde, el valor 'Verde'
const francia: Pais = Pais.Fr // 3 --> Fr tiene como valor autoincremental, desde 2, 3

// Tipo ANY
const miListaCombinada: [number, string, boolean, number] = [1, 'hola', true, 2.4]
const miListaCombinada2: any[] = [1, 'hola', true, 2.4]

// Conversión de tipos con AS
const valor: any = "Esto es un string";
// let strLength: number = valor.length; // ERROR --> Any no tiene Length
const strLength: number = (valor as string).length; // lo tenemos que usar como String

// Parámetros OPCIONALES
function mandarMensaje(mensaje: string, esDebug?: boolean) {
  if (esDebug){ // si se le ha pasado esDebug y es true
    console.log("DEBUG: " + mensaje);
  } else {
    console.log(mensaje); // si no se le ha pasado o es false
  }
}

mandarMensaje("Hola Mundo"); // 'Hola Mundo'
mandarMensaje("probando", true); // 'DEBUG: probando'


interface IProducto {
  ean: string,
  precio: number,
  descripcion?: number // la descripción pasa a ser opcional
}

const consola: IProducto = {
  ean: '123',
  precio: 30,
  // puedo no pasarle la descripción
}


// Interfaces y sobrecarga de funciones
interface MostrarMensaje {
  (mensaje: string): void; // La función es con un string como parámetro
  (mensaje: string[]): void; // La función es con un Array de strings como parámetro
}

const mostrar: MostrarMensaje = (mensaje) => {
  // Si mensaje es un ARRAY
  if (Array.isArray(mensaje)){ 
    console.log (mensaje.join(' '));
  } else { // Si no lo es, es decir es un string
    console.log (mensaje);
  }
}

mostrar("Hola Mundo");
mostrar(["3","5","6"]);




